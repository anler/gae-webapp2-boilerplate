#!/usr/bin/env python
#
# Copyright (c) 2012 Floqq innovation SL. All Right Reserved
#
"""dev_appserver.py helper.

Additional arguments can be passed to dev_appserver.py separating them with a
"--", for example:
    python runserver.py -- --clear_datastore
"""
from __future__ import print_function
import os
import sys
import subprocess
import argparse
import shlex

base = os.path.join(os.path.dirname(__file__), 'stores')

datastore = "{base}/{namespace}/application.datastore"
searchindex = "{base}/{namespace}/application.searchindexes"
blobstore = "{base}/{namespace}/application.blobstore"
logs = "{base}/{namespace}/application.logs"

cmd = ("dev_appserver.py --show_mail_body --high_replication --use_sqlite "
        "--datastore_path={datastore} --search_indexes_path={searchindex} "
        "--blobstore_path={blobstore} {extra_argv} .")


def mkdir_p(path):
    b = os.path.dirname(path)
    if not os.path.exists(b):
        os.makedirs(b)


def run_appserver(namespace, server_argv=None):
    if server_argv is None:
        server_argv = []

    datastore_path = datastore.format(base=base, namespace=namespace)
    searchindex_path = searchindex.format(base=base, namespace=namespace)
    blobstore_path = blobstore.format(base=base, namespace=namespace)
    mkdir_p(datastore_path)
    mkdir_p(searchindex_path)
    mkdir_p(blobstore_path)

    extra_args = " ".join(server_argv)
    command = cmd.format(datastore=datastore_path, searchindex=searchindex_path,
                         blobstore=blobstore_path, extra_argv=extra_args)

    print("Running {0!r}".format(command))

    return subprocess.call(command, shell=True)


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]
    if "--" in argv:
        index = argv.index("--")
        server_argv = argv[index + 1:]
        argv = argv[:index]
    else:
        server_argv = None
    parser = argparse.ArgumentParser(
            description=__doc__,
            formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("namespace", default="default", nargs="?",
                        help=("Namespace in which store datastore, blobstore "
                              "and searchindex data. Defaults to 'default'"))
    args = parser.parse_args(argv)

    return run_appserver(args.namespace, server_argv)


if __name__ == "__main__":
    sys.exit(main())
