import os

import jinja2

from extensions.static import static


templates_path = ('templates',)
extensions = (static(static_path='static'),)

jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader(templates_path),
                               extensions=extensions)


def get_template(template_name):
    return jinja_env.get_template(template_name)
