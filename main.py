from application import RequestHandler, WSGIApplication


class MainHandler(RequestHandler):
    def get(self):
        self.render('base.html')


app = WSGIApplication([
    ('/', MainHandler)
], debug=True)
