import webapp2

from environ import get_template


class RequestHandler(webapp2.RequestHandler):
    def write(self, text):
        self.response.write(text)

    def get_template(self, template_name):
        return get_template(template_name)

    def render(self, template_name, **context):
        template = self.get_template(template_name)
        self.write(template.render(**context))


WSGIApplication = webapp2.WSGIApplication
